## Creative Studio**®** and the QLI and CSQ Formats

The Gammill**®** Creative Studio**®** software is supplied with a number of patterns files. 
These files come in the ".QLI" format. Also, when you save a pattern from Creative Studio,
it is saved in the “.CSQ” format. Wouldn’t it be nice to be able to use those pattern files
in other programs so you could view them, organize them, edit or even create new patterns 
using programs other than Creative Studio**®**.

These ".QLI" and “.CSQ” file formats are claimed by Gammill to be proprietary. That means 
that the true specification of these files can only be guessed by anyone other than Gammill.
Gammill has refused to release specifications for these formats. It’s anyone’s guess as to
why but I think it’s just because they don’t see this as a priority. So only Gammill has the
specification of these file formats and everyone else is left to guess what the format is.
This makes it bad for anyone wanting to provide tools to interoperate with these files since
anyone who writes these tools can only guess what the actual specifications are and only if
everyone guesses the same can these tools work.

Hence, I propose we, the community of Creative Studio**®** users, write our own specifications
for these formats in an "open source" way. This means we can all contribute to these
specifications eventually we can all enjoy the benefits of interoperability with the
patterns we use with other tools. In other words, all tool builders and pattern providers
can claim to speak exactly the same language when it comes to interoperating with these
pattern files.

To start with, I have defined "[CSQV1](https://bitbucket.org/owebeeone/csq/src/master/CSQV1.md)" or the
“[CSQ Open Source FIle Format Specification V1.0](https://bitbucket.org/owebeeone/csq/src/master/CSQV1.md)”.
The CSQV1 specification is relatively simple. It should be relatively simple to create tools
to read and write the format. 


