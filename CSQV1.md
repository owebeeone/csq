## CSQ Open Source FIle Format Specification V1.0 - CSQV1

Author: Gianni Mariani ([gianni@kimquilt.com](mailto:gianni@kimquilt.com))

Permission to copy granted under the [Creative Commons License](http://creativecommons.org/licenses/by/2.0.)

[http://creativecommons.org/licenses/by/2.0](http://creativecommons.org/licenses/by/2.0).

FIles with the ".CSQ" extension are used by the Gammill® Creative Studio® software suite to define quilting “patterns”. The files are basically a collection of 2D lines and splines and arbitrary comments. While Creative Studio® is able to read other file formats (including QLI and DXF formats), CSQ is used as the format to export patterns.

Note that this specification is the "Open Source" specification and may differ in possibly unknown ways to the unpublished Creative Studio® specification. Proposals to update this document are welcome through either bug reports or git push requests.

Any tool builder or CSQ file provider can use the title "CSQ Open Source FIle Format Specification V1.0" or the abbreviation “CSQV1” to denote referring to this document. 

## Why Define The CSQ Format

Ideally, Gammill® would provide this document. There are many examples of companies documenting file formats for interoperability. See the DXF specification from Autodesk or the OOXML specifications from Microsoft.

Formally defining the CSQ file format specification allows anyone to create a compliant application. That means you can expect software from sources other than Gammill® to provide support CSQ files in a consistent way.

Alternatively, not defining the format means everyone developing software that claims to support CSQ format may be making incompatible software and this only leads to problems for end users where they may not reliably interoperate with the applications using these files.

## The CSQ Format

CSQ files provide similar data to a subset of what is supported in [SVG Path](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths)s. The basic primitives are "move", “line to” and “cubic bezier spline to”. CSQ also supports comments which should be used when text search is an application option.

The CSQ file is a text file encoded with [ISO-10646](http://unicode.org/faq/unicode_iso.html) [UTF-8](http://unicode.org/faq/utf_bom.html#UTF8). As per with the standard specification of UTF-8 transmission, the byte order mark is not recommended and should not be used in CSQ files.

### Magic String #CSQLI

The CSQ file begins with a "magic string" “#CSQLI” followed by a new line. This 6 character sequence should begin at the first position in the file. CSQ streams that contain the unicode byte order mark (i.e the character sequence 0xEF,0xBB,0xBF) in the beginning of a CSQ file are malformed.

### White Space

Apart from the first line in the file, white space is ignored but preserved in comments.

### Comments

Comments are denoted by starting with character "#" or the character sequence “NO INFO”. Comments terminate at the end of the line.

### Commands

All lines in a CSQ file have the following grammar. Namely (in a modified BNF specification):

    line : command | comment
    command: x-coordinate y-coordinate command-type [hash_comment]
    x-coordinate: <float> ','
    y-coordinate: <float> ','
    command-type: ('0' | '1' | '3')
    hash_comment: '#' <text>
    comment: ("#" | "NO INFO") <text>

The command-type corresponds as follows:

    ‘0’ : Move to
    ‘1’ : Line to
    ‘3’ : Spline to ;There must be a multiple of 3 spline to coordinates.

Commands that require a start point (Line to and Spline to) use the end point of the previous command. This allows chaining of commands to create a continuous path without the need to repeat the first point.

‘Line to’ produces a straight line segment between the start and end points.

‘Spline to’ is a cubic Bezier spline with the 4 control points being the previous end point (as the start control point) and 3 coordinates provided by 3 consecutive "Spline to" commands.

### Examples

An example CSQ file forming a unit square.

    #CSQLI
    0.0,0.0,0
    1.0,0.0,1
    1.0,1.0,1
    0.0,1.0,1
    0.0,0.0,1


An example CSQ file forming a "hump" curve..

    #CSQLI
    0.0,0.0,0
    0.0,1.0,3
    1.0,1.0,3
    1.0,0.0,3

An example of some comments.

    #CSQLI
    # This is a unit square.
    0.0,0.0,0
    1.0,0.0,1
    1.0,1.0,1
    0.0,1.0,1
    0.0,0.0,1 # This closes the loop.
    NO INFO Permission to copy granted under the Creative Commons License 
    NO INFO http://creativecommons.org/licenses/by/2.0.

## Raising Issues

If you want to contribute to this specification, please raise issues in the issue tracker [https://bitbucket.org/owebeeone/csq/issues?status=new&status=open](https://bitbucket.org/owebeeone/csq/issues?status=new&status=open)
or create a pull request.